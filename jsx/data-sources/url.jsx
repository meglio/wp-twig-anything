var TwigAnythingDataSource_url = React.createClass({
    getInitialState: function() {
        return {
            url: '',
            method: 'GET'
        }
    },
    getSettings: function() {
        return {
            url    : this.state.url,
            method : this.state.method
        }
    },
    handleUrlChange: function(e) {
        this.setState({url: e.target.value});
    },
    handleMethodChange: function(e) {
        this.setState({method: e.target.value});
    },
    render: function() {
        return (
            <table className="form-table">
                <tbody>

                {/* DATA URL */}
                <tr>
                    <th scope="row">
                        <label htmlFor="twig_anything_data_source_url">Data URL</label>
                    </th>
                    <td>
                        <input
                            name     = "twig_anything_data_source_url"
                            id       = "twig_anything_data_source_url"
                            value    = {this.state.url}
                            onChange = {this.handleUrlChange}
                            className = "large-text"/>

                        <p className="description">
                            Full URL to fetch data from, including <code>http:&#47;&#47;</code>
                            or <code>https:&#47;&#47;</code>
                        </p>
                    </td>
                </tr>

                {/* REQUEST METHOD */}
                <tr>
                    <th scope="row">
                        <label htmlFor="twig_anything_data_source_method">HTTP Request Method</label>
                    </th>
                    <td>
                        <select
                            name     = "twig_anything_data_source_method"
                            id       = "twig_anything_data_source_method"
                            value    = {this.state.method}
                            onChange = {this.handleMethodChange}>
                            <option value="GET">GET</option>
                            <option value="POST">POST</option>
                        </select>

                        <p className="description">
                            Which HTTP method to use when fetching data from URL
                        </p>
                    </td>
                </tr>

                </tbody>
            </table>
        )
    }
});