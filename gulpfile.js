var path = require('path');
var gulp = require('gulp');
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('react', function () {

    // All JSX files
    gulp.src([
            'jsx/**/*.jsx'
        ])
        .pipe(babel({
            presets: ['es2015', 'react'],
            comments: false
        }))
        .pipe(gulp.dest('plugin_src/jsx/'));
});


gulp.task('watch', function () {
    gulp.watch('jsx/**/*.jsx', ['react']);
});