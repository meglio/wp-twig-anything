var TwigAnythingDataSource_file = React.createClass({
    getInitialState: function() {
        return {
            is_file_path_absolute: false,
            file_path: ''
        }
    },
    getSettings: function() {
        return {
            is_file_path_absolute: this.state.is_file_path_absolute,
            file_path: this.state.file_path
        }
    },
    handleIsFilePathAbsoluteChange: function(e) {
        this.setState({is_file_path_absolute: e.target.checked})
    },
    handleFilePathChange: function(e) {
        this.setState({file_path: e.target.value});
    },
    render: function() {
        return (
            <table className="form-table">
                <tbody>
                <tr>
                    <th scope="row">File Path</th>
                    <td>
                        <fieldset>
                            <legend className="screen-reader-text">
                                <span>File Path</span>
                            </legend>
                            <input
                                name="twig_anything_data_source_file_path"
                                id="twig_anything_data_source_file_path"
                                value={this.state.file_path}
                                onChange={this.handleFilePathChange}
                                className="large-text"/>
                            <br/>
                            <label htmlFor="twig_anything_data_source_is_file_path_absolute">
                                <input
                                    name="twig_anything_data_source_is_file_path_absolute"
                                    id="twig_anything_data_source_is_file_path_absolute"
                                    type="checkbox"
                                    checked={this.state.is_file_path_absolute}
                                    onChange={this.handleIsFilePathAbsoluteChange} />
                                    File path is absolute
                            </label>
                        </fieldset>
                        <p className="description">
                            If the path is not absolute, it is relative to
                            the root of the WordPress installation:
                            <code>{this.props.wpHomePath}</code>
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        )
    }
});