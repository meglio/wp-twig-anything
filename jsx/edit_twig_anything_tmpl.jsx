var TwigAnythingEmptyReactClass = React.createClass({
    render: function() {
        return (<div></div>);
    }
});

var DataSourceMetaBox = React.createClass({
    propTypes: {
        dataSources: React.PropTypes.array.isRequired,
        formats: React.PropTypes.array.isRequired,
        wpHomePath: React.PropTypes.string.isRequired
    },
    getInitialState: function() {
        return {
            source_type: 'empty',
            format: 'raw',
            cache_seconds: '',
            // Also: use_cache_or_display_error, always_display_error
            on_data_error: 'use_cache_or_display_nothing'
        };
    },
    setDataSourceState: function(state, callback) {
        if (!callback) {
            callback = function() {};
        }
        this.refs['data_source_settings'].setState(state, callback);
    },
    setFormatState: function(state, callback) {
        if (!callback) {
            callback = function() {};
        }
        this.refs['format_settings'].setState(state, callback);
    },
    handleSourceTypeChange: function(e) {
        var newSourceType = e.target.value;
        var newState = {
            'source_type': newSourceType
        };
        if (newSourceType == 'empty') {
            newState.format = 'raw';
            newState.cache_seconds = '';
        }
        this.setState(newState);
    },
    handleFormatChange: function(e) {
        this.setState({format: e.target.value});
    },
    handleCacheSecondsChange: function(e) {
        this.setState({cache_seconds: e.target.value});
    },
    handleOnDataErrorChange: function(e) {
        this.setState({on_data_error: e.target.value});
    },
    render: function() {
        // Custom data source settings
        var dataSourceReactClass = window['TwigAnythingDataSource_' + this.state.source_type];
        if (typeof dataSourceReactClass == "undefined") {
            dataSourceReactClass = TwigAnythingEmptyReactClass;
        }
        var dataSourceSettings = React.createElement(
            dataSourceReactClass,
            {
                ref: 'data_source_settings',
                wpHomePath: this.props.wpHomePath
            }
        );

        // Custom format settings
        var formatReactClass = window['TwigAnythingFormat_' + this.state.format];
        if (typeof formatReactClass == "undefined") {
            formatReactClass = TwigAnythingEmptyReactClass;
        }
        var formatSettings = React.createElement(
            formatReactClass,
            {
                ref: 'format_settings',
                wpHomePath: this.props.wpHomePath
            }
        );

        var i;
        var dataSourceOptions = [];
        var dataSourceDescription = '';
        for (i = 0; i < this.props.dataSources.length; i++) {
            var ds = this.props.dataSources[i];
            dataSourceOptions.push(
                <option key={ds.slug} value={ds.slug}>{ds.longName}</option>
            );
            if (ds.slug == this.state.source_type) {
                dataSourceDescription = ds.description;
            }
        }

        // Formats dropdown options
        var formatsOptions = [];
        var formatDescription = '';
        for (i = 0; i < this.props.formats.length; i++) {
            var format = this.props.formats[i];
            formatsOptions.push(
                <option key={format.slug} value={format.slug}>{format.longName}</option>
            );
            if (format.slug == this.state.format) {
                formatDescription = format.description;
            }
        }

        return (
            <div>
                <table className="form-table">
                    <tbody>

                    {/* SOURCE TYPE */}
                    <tr>
                        <th scope="row">
                            <label htmlFor="twig_anything_source_type">Source Type</label>
                        </th>
                        <td>
                            <select
                                name     = "twig_anything_source_type"
                                id       = "twig_anything_source_type"
                                value    = {this.state.source_type}
                                onChange = {this.handleSourceTypeChange}>
                                {dataSourceOptions}
                            </select>
                            <p className="description">
                                {dataSourceDescription}
                            </p>
                        </td>
                    </tr>

                    {/* DATA FORMAT */}
                    <tr>
                        <th scope="row">
                            <label htmlFor="twig_anything_format">Data Format</label>
                        </th>
                        <td>
                            <select
                                name     = "twig_anything_format"
                                id       = "twig_anything_format"
                                value    = {this.state.format}
                                onChange = {this.handleFormatChange}>
                                {formatsOptions}
                            </select>
                            <p className="description">
                                {formatDescription}
                            </p>
                        </td>
                    </tr>

                    {/* CACHE SECONDS */}
                    <tr>
                        <th scope="row">
                            <label htmlFor="twig_anything_cache_seconds">Cache lifetime in seconds</label>
                        </th>
                        <td>
                            <input
                                name     = "twig_anything_cache_seconds"
                                id       = "twig_anything_cache_seconds"
                                value    = {this.state.cache_seconds}
                                onChange = {this.handleCacheSecondsChange}/>
                            <p className="description">
                                Leave empty to avoid both reading from and writing to cache
                            </p>
                        </td>
                    </tr>

                    <tr>
                        <th scope="row">
                            <label htmlFor="twig_anything_on_data_error">Data errors handling</label>
                        </th>
                        <td>
                            <select
                                name     = "twig_anything_on_data_error"
                                id       = "twig_anything_on_data_error"
                                value    = {this.state.on_data_error}
                                onChange = {this.handleOnDataErrorChange}>
                                <option value="use_cache_or_display_nothing">
                                    Use cache or display nothing
                                </option>
                                <option value="use_cache_or_display_error">
                                    Use cache or display error
                                </option>
                                <option value="always_display_error">
                                    Always display error
                                </option>
                            </select>
                            <p className="description">
                                What to do if data cannot be retrieved or parsed?<br/>
                                · <code>use cache or display nothing</code> -
                                try using data from out-of-date cache if it exists,
                                otherwise display nothing (an empty string)<br/>
                                · <code>use cache or display error</code> -
                                try using data from out-of-date cache if it exists,
                                otherwise display an error message<br/>
                                · <code>always display error</code> -
                                always display an error message
                            </p>
                        </td>
                    </tr>

                    </tbody>
                </table>

                {/* DATA SOURCE SETTINGS */}
                {dataSourceSettings}

                {/* FORMAT SETTINGS */}
                {formatSettings}
            </div>
        );
    }
});

jQuery(function() {
    // Add some hint information
    jQuery("#wp-word-count").after(
        jQuery("<td>").attr('id', 'twig-anything-hotkeys-hint').append(
            jQuery("<strong>").text("Ctrl-Enter"),
            jQuery("<span>").text(" - fullscreen mode")
        )
    );

    // Custom CodeMirror mode that combines HTML and Twig
    TwigAnythingCodeMirrorWrapper.CodeMirror.defineMode("twig_html", function (config) {
        return TwigAnythingCodeMirrorWrapper.CodeMirror.multiplexingMode (
            TwigAnythingCodeMirrorWrapper.CodeMirror.getMode(config, "text/html"), {
                open: /\{[\{%#]/, close: /[}%#]\}/,
                mode: TwigAnythingCodeMirrorWrapper.CodeMirror.getMode(config, "twig"),
                parseDelimiters: true
            });
    });

    // Initialize CodeMirror
    var contentCodeMirror = TwigAnythingCodeMirrorWrapper.CodeMirror.fromTextArea(document.getElementById("content"), {
        mode:  "twig_html",
        lineNumbers: true,
        indentUnit: 2,
        tabSize: 2,
        indentWithTabs: false,
        viewportMargin: Infinity,
        placeholder: "Enter your HTML template here...",
        extraKeys: {
            "Ctrl-Enter": function(cm) {
                cm.setOption("fullScreen", !cm.getOption("fullScreen"));
            },
            "Esc": function(cm) {
                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            },
            // Map the Tab key to insert spaces instead of a tab character.
            "Tab": function(cm) {
                cm.replaceSelection('  ');
            }
        }
    });

    // Initialized in PHP
    var input = twigAnythingDataSourceMetaBoxInputData || {
            commonSettings: {},
            dataSourceSettings: {},
            formatSettings: {},
            dataSourcesMeta: [],
            formatsMeta: [],
            wpHomePath: ''
        };

    // Render DataSource metabox
    ReactDOM.render(
        <DataSourceMetaBox
            dataSources={input.dataSourcesMeta}
            formats={input.formatsMeta}
            wpHomePath={input.wpHomePath}/>,
        document.getElementById('data_source_react_container'),
        function () {
            var rootComponent = this;

            function updateFormatState() {
                if (input.formatSettings) {
                    rootComponent.setFormatState(input.formatSettings);
                }
            }

            {/* Common settings are required. If not present,
                no other settings can be set. */}
            if (input.commonSettings) {
                rootComponent.setState(input.commonSettings, function() {

                    {/* Only after common settings are set,
                        can we set data source and format settings */}

                    {/* If there are data source settings, update them first */}
                    if (input.dataSourceSettings) {
                        rootComponent.setDataSourceState(input.dataSourceSettings, function() {
                            updateFormatState();
                        });
                    }

                    else {
                        {/* If there are NO data source settings, update format settings immediately */}
                        updateFormatState();
                    }
                });
            }
        }
    );

    // Copy shortcode to clipboard
    var clipboardClient = new Clipboard('#twig-anything-copy-shortcode-to-clipboard');

    clipboardClient.on('success', function(e) {
        e.clearSelection();

        jQuery("#twig-anything-copy-shortcode-to-clipboard-copied")
            .show()
            .delay(3000)
            .hide(4000)
    });

    clipboardClient.on('error', function(e) {
        if (console && console.error) {
            console.error(
                "Could not copy to clipboard, Action: ",
                e.action,
                ', Trigger: ',
                e.trigger
            );
        }
    });
});